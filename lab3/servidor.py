# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket
import select
import sys
import multiprocessing

# Definindo aspectos básicos da conexão
HOST = ''
PORTA = 6000

# Definindo lista de I/O
entradas = [sys.stdin]
conexoes = {}

# Camada de dados
def dados(nomeArq):
	try:
		f = open(nomeArq, "r", encoding="utf-8")
	except OSError: # Se o arquivo não for encontrado
		return None # Age como sinal de erro

	conteudo = f.read() # String com o conteúdo do arquivo
	f.close()
	return conteudo

# Camada de processamento
def iniciarServer():
	# Inicializar o socket
	sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	sock.bind((HOST,PORTA))

	# Aguarda um cliente
	sock.listen(5) # Dessa vez o server pode receber múltiplos clientes
	sock.setblocking(False) # É importante que o servidor não seja bloqueante para atender requisições de múltiplos clientes

	entradas.append(sock) # Necessário para atender o socket usando o select

	return sock

def aceitarConexao(sock):
	novoSock, endereco = sock.accept()
	conexoes[novoSock] = endereco
	print("Conexão aceita do endereço", endereco[0])

	return novoSock, endereco

def atenderCliente(sock, endereco):
	while True:
		nomeArqBytes = sock.recv(1048) # Recebe o nome do arquivo da camada de interface
		nomeArqStr = str(nomeArqBytes, encoding="utf-8")
		palavraBytes = sock.recv(1048) # Recebe a palavra da camada de interface

		if nomeArqStr == "0" or not nomeArqBytes: # Se 0, a string é interpretada como um sinal de fim
			print(str(endereco), "encerrou conexão")
			sock.close()
			return

		# Se comunica com a camada de dados
		conteudo = dados(nomeArqStr) # Recebe conteúdo do arquivo da camada de dados

		# Processa o arquivo
		if conteudo == None: # Situação onde o arquivo não é encontrado
			retorno = -1
		else: # Conta as palavras
			palavraStr = str(palavraBytes, encoding="utf-8")
			retorno = conteudo.count(palavraStr)

		# Envia os resultados ao cliente
		sock.send(retorno.to_bytes(4, "big", signed=True)) # Usamos o cast int() para garantir que o valor está no formato correto


sockPrincipal = iniciarServer()
clientes = []
print("Servidor inicializado, aguardando conexões")

# Laço principal, espera por entradas
while True:
	leitura, escrita, excecao = select.select(entradas, [], [])

	for pronto in leitura:
		if pronto == sockPrincipal: # Pedido novo de conexão
			novoSock, endereco = aceitarConexao(sockPrincipal)
			cliente = multiprocessing.Process(target=atenderCliente, args=(novoSock,endereco))
			cliente.start()
			clientes.append(cliente)

		elif pronto == sys.stdin:
			comando = input()
			if comando == "FIM": # Encerra a execução
				for c in clientes: # Aguarda o fim dos processos atuais
					c.join()
				sockPrincipal.close()
				sys.exit()
