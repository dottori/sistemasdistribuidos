# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket

HOST = 'localhost'
PORTA = 7000

# Inicializar o socket
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.connect((HOST,PORTA))

# Rotina de transmissão
print("Conectado. Iniciando servidor de echo.")
print("Para encerrar, digite FIM.")
while True:
	msg = input()
	sock.send(bytes(msg,'utf-8')) # É necessário codificar a mensagem em bytes para poder enviá-la
	if msg == "FIM":
		break
	volta = sock.recv(1024)
	print("Recebido", str(volta, encoding='utf-8'))

print("Recebido sinal de FIM. Encerrando.")

sock.close()
