# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket

HOST = ''
PORTA = 7000

# Inicializar o socket
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.bind((HOST,PORTA))
sock.listen(1)
novoSock, endereco = sock.accept()
print("Socket ativado no endereço", endereco[0])

# Rotina de transmissão
msg = "placeholder"
while msg:
	msg = ""
	msg = novoSock.recv(1024)
	if msg == "FIM" or not msg:
		break
	novoSock.send(msg)

print("Recebido sinal de FIM. Encerrando.")

novoSock.close()
sock.close()
