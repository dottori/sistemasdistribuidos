# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket

# Implementação da camada de dados
def dados(nomeArq):
	try:
		f = open(nomeArq, "r", encoding="utf-8")
	except OSError: # Se o arquivo não for encontrado
		return None # Age como sinal de erro

	conteudo = f.read() # String com o conteúdo do arquivo
	f.close()
	return conteudo

# Implementação da camada de processamento
def processamento():
	# Definindo aspectos básicos da conexão
	HOST = ''
	PORTA = 6000

	# Inicializar o socket
	sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	sock.bind((HOST,PORTA))

	while True:
		# Aguarda um cliente
		sock.listen(1) # O servidor só aceita um cliente por vez

		# Aceita um cliente, se esse chegar
		novoSock, endereco = sock.accept()
		print("Socket ativado no endereço", endereco[0])

		# Rotina de transmissão
		while True:
			nomeArqBytes = novoSock.recv(1048) # Recebe o nome do arquivo da camada de interface
			nomeArqStr = str(nomeArqBytes, encoding="utf-8")
			palavraBytes = novoSock.recv(1048) # Recebe a palavra da camada de interface

			if nomeArqStr == "0" or not nomeArqBytes: # Se 0, a string é interpretada como um sinal de fim
				break

			# Se comunica com a camada de dados
			conteudo = dados(nomeArqStr) # Recebe conteúdo do arquivo da camada de dados

			# Processa o arquivo
			if conteudo == None: # Situação onde o arquivo não é encontrado
				retorno = -1
			else:
				palavraStr = str(palavraBytes, encoding="utf-8")
				retorno = conteudo.count(palavraStr)

			# Envia os resultados ao cliente
			novoSock.send(retorno.to_bytes(4, "big", signed=True)) # Usamos o cast int() para garantir que o valor está no formato correto

		print("Recebido sinal de fim. Encerrando conexão atual. Aguardando nova conexão.")

		novoSock.close()

processamento()
