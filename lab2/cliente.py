# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket

# Define aspectos básicos da conexão
HOST = 'localhost'
PORTA = 6000

# Inicializar o socket
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.connect((HOST,PORTA))
print("Conectado.")

# Rotina de transmissão
while True:
	nome = input("Digite o nome do arquivo (digite 0 para encerrar): ")
	sock.send(bytes(nome,'utf-8'))
	if nome == "0": # Sinal que indica fim do processo
		break

	palavra = input("Digite a palavra a ser buscada: ")
	sock.send(bytes(palavra,'utf-8'))

	resultadoBytes = sock.recv(4)
	resultadoInt = int.from_bytes(resultadoBytes, "big", signed="True")

	if resultadoInt == -1:
		print("O arquivo não foi encontrado.")
	elif resultadoInt == 0:
		print("Não foi encontrada nenhuma ocorrência da palavra no arquivo.")
	elif resultadoInt == 1:
		print("Foi encontrada 1 ocorrência da palavra no arquivo.")
	elif resultadoInt > 1:
		print("Foram encontradas %d ocorrências da palavra no arquivo." % (resultadoInt))

	print()

print("Recebido sinal de fim. Encerrando.")

sock.close()
