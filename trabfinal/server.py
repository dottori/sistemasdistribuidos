# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket
import select
import sys
import multiprocessing
import time

# Códigos das mensagens
CODIGO_SUBSC = '0'
CODIGO_PUB_N_PERSIST = '1'
CODIGO_PUB_S_PERSIST = '2'
CODIGO_CANCEL = '3'
CODIGO_SAIR = '4'

# Propriedades da conexão
HOST = ''
PORTA = 6004
entradas = [sys.stdin]
conexoes = {}

# Abre e configura o socket principal
def iniciarServer():
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind((HOST, PORTA))

	# Aguarda clientes
	sock.listen(20)
	sock.setblocking(False)

	entradas.append(sock)

	return sock

# Inicia uma conexão nova
def aceitarConexao(sock):
	novoSock, endereco = sock.accept()
	novoSock.setblocking(False)
	conexoes[novoSock] = endereco
	print("Conexão aceita do endereço", endereco[0])
	return novoSock, endereco

# Laço principal das threads filhas
def atenderCliente(sock, endereco, filaRecebimento, filaPublicacao):
	topicos = []
	sair = False

	while not sair:
		# Recebe publicações, se houver novas
		while not filaRecebimento.empty():
			pub = filaRecebimento.get()
			codigo = pub[0]
			tempStr = pub[1:].split('\n', 1)
			topico = tempStr[0]
			mensagem = tempStr[1]
			if topico.upper() in topicos:
				sock.send(bytes(pub,'utf-8'))

		# Processa mensagens do cliente
		try:
			mensCliente = sock.recv(4096)
			mensClienteStr = str(mensCliente, encoding="utf-8")
			codigoAcao = mensClienteStr[0]

			if codigoAcao == CODIGO_SUBSC:
				topico = mensClienteStr[1:].upper()
				if not topico in topicos:
					topicos.append(topico)

			elif codigoAcao == CODIGO_CANCEL:
				topico = mensClienteStr[1:].upper()
				if topico in topicos:
					topicos.remove(topico)

			elif codigoAcao == CODIGO_PUB_N_PERSIST or codigoAcao == CODIGO_PUB_S_PERSIST:
				filaPublicacao.put(mensClienteStr)

			elif codigoAcao == CODIGO_SAIR:
				print("Cliente", sock, "encerrou.")
				sair = True
		except:
			pass

		time.sleep(0.2)

sockPrincipal = iniciarServer()
clientes = []
filasRecebimento = []
filasPublicacao = []
print("Servidor inicializado, aguardando conexões")

filaProcessada = 0
while True:
	leitura, escrita, excecao = select.select(entradas, [], [], 0.1)

	for pronto in leitura:
		# Cria nova conexão
		if pronto == sockPrincipal:
			novoSock, endereco = aceitarConexao(sockPrincipal)
			print("Conexão aberta em", novoSock, ",", endereco)

			# Cria duas filas por cliente novo
			novaFilaRecebimento = multiprocessing.Queue()
			novaFilaPublicacao = multiprocessing.Queue()
			filasRecebimento.append(novaFilaRecebimento)
			filasPublicacao.append(novaFilaPublicacao)

			cliente = multiprocessing.Process(target=atenderCliente, args=(novoSock,endereco,novaFilaRecebimento,novaFilaPublicacao))
			cliente.start()
			clientes.append(cliente)

		# Processa sinal de saída e encerra
		elif pronto == sys.stdin:
			comando = input()
			if comando == "FIM":
				for c in clientes:
					c.join()
				sockPrincipal.close()
				sys.exit()

	# Repassa mensagens pendentes para as demais threads, se houver
	# Usamos um contador (filaProcessada) para que não seja necessário
	# esperar a leitura de todas as filas até retornar ao select
	# Desse modo, só uma fila será lida por laço
	if len(filasPublicacao) > 0:
		while not filasPublicacao[filaProcessada%len(filasPublicacao)].empty():
			pub = filasPublicacao[filaProcessada%len(filasPublicacao)].get()
			for fila in filasRecebimento:
				fila.put(pub)
	filaProcessada = filaProcessada+1
