# Rodrigo Dottori de Oliveira
# DRE: 116015658

import socket
import queue
import select
import sys

print("Bem-vindo ao Sistema de Publicação de Anúncios")

# Propriedades da conexão
HOST = 'localhost'
PORTA = 6004

# Códigos dos comandos
CODIGO_SUBSC = '0'
CODIGO_PUB_N_PERSIST = '1'
CODIGO_PUB_S_PERSIST = '2'
CODIGO_CANCEL = '3'
CODIGO_SAIR = '4'

# Inicializa a conexão
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.connect((HOST,PORTA))
sock.setblocking(0)
print("Conectado.")

filaMensagens = queue.Queue()
entradas = [sys.stdin, sock]

topicos = []

print("Escolha uma opção:")
print("1. Se inscrever em um tópico.")
print("2. Cancelar uma inscrição.")
print("3. Publicar um anúncio.")
print("4. Ler mensagens novas.")
print("5. Ver inscrições atuais.")
print("6. Sair.")

while True:
	leitura, escrita, excecao = select.select(entradas, [], [])
	for pronto in leitura:
		# Recebe mensagem
		if pronto == sock:
			recebida = sock.recv(4096)
			if recebida:
				print("Mensagem nova recebida!")
				filaMensagens.put(str(recebida, encoding="utf-8"))

		# Processa entradas do usuário
		elif pronto == sys.stdin:
			comando = input()

			# Inscrição em tópico
			if comando == "1":
				topico = input("Entre com o tópico no qual quer se inscrever: ")
				topico = topico.upper()

				if not topico in topicos:
					topicos.append(topico)
					aEnviar = CODIGO_SUBSC + topico
					sock.send(bytes(aEnviar,'utf-8'))
					print("Você agora está inscrito(a) no tópico", topico)

				else:
					print("O cliente já está inscrito nesse tópico.")

				print("")

			# Cancelamento de inscrição
			elif comando == "2":
				topico = input("Entre com o tópico cuja inscrição deseja cancelar: ")
				topico = topico.upper()

				if topico in topicos:
					topicos.remove(topico)
					aEnviar = CODIGO_CANCEL + topico
					sock.send(bytes(aEnviar,'utf-8'))
					print("Você não está mais inscrito(a) no tópico", topico)

				else:
					print("Você não está inscrito(a) nesse tópico.")

				print("")

			# Publicação de anúncio
			elif comando == "3":
				topico = input("Entre com o tópico do anúncio: ")
				corpoMensagem = input("Entre com o corpo do anúncio: ")
				aEnviar = CODIGO_PUB_N_PERSIST + topico + '\n' + corpoMensagem
				sock.send(bytes(aEnviar,'utf-8'))
				print("Anúncio publicado.")
				print("")

			# Leitura de mensagens
			elif comando == "4":
				if filaMensagens.empty():
					print("Não há mensagens novas.")
					print("")

				while not filaMensagens.empty():
					aImprimir = filaMensagens.get()
					imprTemp = aImprimir[1:].split('\n',1)
					topico = imprTemp[0]
					corpoMensagem = imprTemp[1]
					print("Tópico: ", topico)
					print("Mensagem: ", corpoMensagem)
					print("")

			# Imprime inscrições atuais
			elif comando == "5":
				print("Você está inscrito(a) nos seguintes tópicos:")
				for topico in topicos:
					print(topico)
				print("")

			# Encerra
			elif comando == "6":
				aEnviar = str(CODIGO_SAIR)
				sock.send(bytes(aEnviar,'utf-8'))
				sock.close()
				sys.exit()

			elif comando == '\n' or comando == ' ' or comando == '':
				pass

			else:
				print("Comando inválido. Por favor tente novamente.")
				print("")

			print("Escolha uma opção:")
			print("1. Se inscrever em um tópico.")
			print("2. Cancelar uma inscrição.")
			print("3. Publicar um anúncio.")
			print("4. Ler mensagens novas.")
			print("5. Ver inscrições atuais.")
			print("6. Sair.")
